:PenConname: Penguincon 2020
:PenContag: #penguincon
:PenCondates: April 24 - 26
:PenConsite: https://2020.penguicon.org/
:PenConlocation: Southfield, MI, US
:PenConupdateurl: https://2020.penguicon.org/2020/04/moving-forward/
:PenConupdatestatus: virtual ( as of Apr 07 )
:PenConupdatenotes: registration cancellation possible, else applied to 2021
{PenConsite}[{PenConname}] - {PenConlocation}::
  * {PenCondates}
  * {PenConupdateurl}[{PenConupdatestatus}] - {PenConupdatenotes}
